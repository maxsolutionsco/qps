# -*- coding: utf-8 -*-

from odoo import models, fields, api, _, exceptions
import random


class RecursosMaterialesCategoria(models.Model):
    _name = 'recursos_materiales.categoria'
    _description = 'Recursos materiales categoria'

    _rec_name = 'nombre_mostrar'

    nombre = fields.Char(string=_("Nombre"))
    codigo = fields.Char(string=_("Código"))
    nombre_mostrar = fields.Char(string=_("Nombre mostrar"),
                                 compute="_compute_nombre_mostrar")
    color = fields.Integer(string=_("Color kanban"), default=lambda self: self._default_color())
    es_favorito = fields.Boolean(string=_("Favorito"), default=lambda self: self._default_favorito())
    imagen = fields.Binary(string=_("Imagen"))

    _sql_constraints = [
        # ('nombre de la restriccion', 'operador', 'mensaje a mostrar')
        ('codigo_restriccion_unico', 'UNIQUE(codigo)', _('El código debe ser único'))
    ]

    @api.model
    def _default_favorito(self):
        return random.randint(0, 1)

    @api.model
    def _default_color(self):
        # kanban_color ([0-9])
        return random.randint(0, 9)

    @api.depends('nombre', 'codigo')
    @api.multi
    def _compute_nombre_mostrar(self):
        for registro in self:
            registro.nombre_mostrar = str(registro.codigo) + '-' + str(registro.nombre)

    @api.onchange('nombre')
    def _onchange_nombre(self):
        # Método search
        # arg1 = [(c1),(c2), (c...) , ('campo del modelo', 'operador', 'valor')]
        # arg2 = limit **kwargs [top 10, limit 10]
        # arg2 = order
        # return : object
        if self.nombre:
            registros = self.env[self._name].search(
                [('nombre', '=', self.nombre)]
                # , limit=10
                # , order='create_date, create_uid'
            )
            if len(registros) > 0:
                # registros[0].[nombre, codigo]
                raise exceptions.ValidationError(_("La categoria ya existe"))

        # self.env['recursos_materiales.categoria']
        """
        
            self.env
            self.env.user
            self.env.uid
            self.env[ 'nombre del modelo' ]
         
        """
    @api.model
    def create(self, vals):
        id = super(RecursosMaterialesCategoria, self).create(vals)
        #self.env.context {'tz':'', 'uid: id_user}
        #self.with_context(create=1, primera_creacion='1')
        return id

    @api.multi
    def write(self, vals):
        if vals.get('nombre', False):
            if self._validacion_nombre(vals.get('nombre')):
                vals.update({'nombre': vals.get('nombre') + " - categoria"})
                self.copy()
        return super(RecursosMaterialesCategoria, self).write(vals)

    @api.multi
    def unlink(self):
        oficios_ids = self.env['recursos_materiales.oficio'].search([])
        for oficio in oficios_ids:
            if self.id in oficio.categoria_id.ids:
                raise exceptions.MissingError("La categoria tiene un relacion en Oficio")
            for oficio_linea in oficio.oficio_linea_ids:
                if self.id == oficio_linea.categoria_id.id:
                    raise exceptions.MissingError(msg=_("La categoria tiene una relacion en Oficio Lineas"))
        return super(RecursosMaterialesCategoria, self).unlink()

    @api.model
    def _validacion_nombre(self, nombre):
        return len(nombre) > 5

    @api.model
    def __render(self, xml):
        return str(xml).replace('#{}', "odoo.kanban_color(10)")
