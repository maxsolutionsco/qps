# -*- coding: utf-8 -*-

from odoo import models, fields, api, _, exceptions
import uuid

class RecursosMaterialesArea(models.Model):
    _name = 'recursos_materiales.area'  # recursos_materiales_area
    _table = 'rm_area'

    _rec_name = 'nombre'

    nombre = fields.Char(string=_("Nombre"))
    active = fields.Boolean(string=_("Estatus"), default=True, copy=False)
    folio = fields.Many2one(comodel_name='ir.sequence', string=_("Folio"))

    @api.constrains('nombre')
    def _constrains_nombre(self):
        # return : int
        if self.nombre:
            registros = self.env[self._name].search_count([('nombre', '=', self.nombre)])
            if registros > 1:
                raise exceptions.ValidationError(_("El area ya existe"))

    @api.onchange('folio')
    def _onchange_folio(self):

        """
            env.ref
            env.user
            env.context
            env.cr
        """
        ## cr
        print(self.env.user.id, self.env.user.name)
        print(self.env.user.has_group('base.group_system'))
        print(self.env.context.get('tz'))
        self.env.cr.execute("select id from res_groups")
        # registros  = self.env.cr.fetchone() []
        # registros  = self.env.cr.dictfetchall() [{}]
        # registros  = self.env.cr.dictfetchone() {}
        total_registros = self.env.cr.rowcount

        ### Browse
        # search([('id', 'in',registros), () ...]) -> [12,3,3,3,4,4,4,] -> [obj, obj, obj]
        # browse([1,2,,3,3,3,3,3,3])
        if total_registros > 0:
            registros = self.env.cr.fetchall() # [(id,nombre ...), ()]
            registros = [registro[0] for registro in registros]
            grupos = self.env['res.groups'].browse(registros)
            for grupo in grupos:
                print(grupo.name)
            print(grupos)

        ## Context
        print(self.env.context)
        # context_copy = self.env.context.copy()
        # context_copy.update({'area': 'Nuevo area'})
        # print(context_copy)
        ## **kwarg
        self.env.context = self.with_context(area="Nuevo area 2").env.context
        print(self.env.context)

        categorias = self.env['recursos_materiales.categoria'].with_context(
            categoria=[]
        ).obtener_categoria()
        print(categorias)

        ## sudo
        ## self.env.user
        ## self.env['recursos_materiales.oficio'].create({})
        ## self.sudo()
        asunto = "Asunto - {}".format(str(uuid.uuid4()))
        #usuario2 = self.env['res.users'].search([('login','=','usuario2')])
        #self.env['recursos_materiales.oficio'].sudo(user=usuario2).create({'asunto': asunto})
        return

    @api.model
    def create(self, vals):
        if not self.env.user.has_group('base.group_system'):
            raise exceptions.AccessError("No tiene permiso de crear areas")
        elif self.env.user.id != 1:
            raise exceptions.AccessError("Solo el usarios pueden crear areas")
        return super(RecursosMaterialesArea, self).create(vals)


class RecursosMaterialesCategoriaMetodo(models.Model):
    _inherit = 'recursos_materiales.categoria'

    @api.model
    def obtener_categoria(self):
        ids = self.env.context.get('categoria', [])
        condicion = []
        if len(ids):
            condicion.append(('id', 'in', []))
        return self.search(condicion)
