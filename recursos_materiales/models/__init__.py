# -*- coding: utf-8 -*-

from . import recursos_materiales_categoria
from . import recursos_materiales_area
from . import recursos_materiales_oficio
from . import recursos_materiales_oficio_linea

from . import res_partner
from . import compras_cfdi