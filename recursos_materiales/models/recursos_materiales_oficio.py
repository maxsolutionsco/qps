# -*- coding: utf-8 -*-

from odoo import models, fields, api, _, exceptions
import re
import random

"""
class Estatus (models.Model):
    _name = 'recursos_materiales.status'

    nombre = fields.Char(string=_("Nombre"))
    valor = fields.Char(string=_("Valor"))
    uso = fields.Char(string=_("Uso"))

   
        nombre: borrador 
        valor: draft 
        uso: recursos_materiales.oficio
        
        nombre: 
        valor: start
        uso: recursos_materiales.ventas
        
    
"""


class RecursosMaterialesOficio(models.Model):
    _name = 'recursos_materiales.oficio'
    _rec_name = 'nombre_mostrar'

    _inherit = ['mail.thread', 'mail.activity.mixin']

    # Datos generales
    name = fields.Char(string=_("Nombre"))

    """ status default:
            draft 
            confirm 
            done
    """
    state = fields.Selection(selection=[
        ('borrador', _('Borrador')),
        ('pendiente', _('Pendiente')),
        ('pendiente_contestar', _('Pendiente por contestar')),
        ('finalizado', _('Finalizado')),
        ('cancelado', _("Cancelado")),
        ('rechazado', _("Rechazado"))
    ], string=_("Estatus"), default="borrador",
        track_visibility='onchange')

    folio = fields.Char(string=_("Folio"), track_visibility='always')  # Secuencia

    fecha = fields.Date(string=_("Fecha"))

    """ default 
        id
        create_date
        write_date
        create_uid 
        write_uid
    """

    fecha_finalizado = fields.Datetime(string=_("Fecha finalizado"))

    empleado = fields.Many2one(comodel_name='res.users', string=_("Empleado"))
    correo_empleado = fields.Char(string=_("Correo electrónico del empleado"),
                                  related='empleado.login', readonly=True)

    # datos solicitante
    nombre_solicitante = fields.Char(string=_("Nombre del solicitante"))
    correo_solicitante = fields.Char(string=_("Correo electrónico del solicitante"))
    # Queja
    asunto = fields.Char(_("Asunto"), pleaceholder=_("Asunto de la queja"))

    area_id = fields.Many2one(comodel_name='recursos_materiales.area',
                              string=_("Área"), default=lambda self: self._default_area())

    categoria_id = fields.Many2many(comodel_name='recursos_materiales.categoria',
                                    string=_("Categoría"))

    queja = fields.Html(string=_("Queja"))

    evidencia = fields.Many2many(comodel_name="ir.attachment",
                                 relation="oficio_ir_attachment_relation",
                                 column1="oficio_id",
                                 column2="attachment_id", string=_("Evidencia"))

    nombre_mostrar = fields.Char(compute="_compute_nombre_mostrar")
    """
         Page / Lineas 
        one2many
        Registro 1 [oficio ] 
            Line 1 [oficio_linea] oficio_id = 1
            Line 2 [oficio_linea] oficio_id = 1
            Line 3 [oficio_linea] oficio_id = 1 
            
    """

    oficio_linea_ids = fields.One2many(
        comodel_name='recursos_materiales.oficio_linea',
        inverse_name='oficio_id', string=_("Lineas"),
        default=lambda self: self._default_lines()
    )

    moneda_id = fields.Many2one(comodel_name='res.currency',
                                string=_("Moneda"),
                                default=lambda self: self.env.user.company_id.currency_id.id)
    total = fields.Monetary(strign=_("Total"), compute="_compute_total", store=True,
                            currency_field='moneda_id')
    area_list = fields.Html(string="Lista de areas", readonly=True, compute='_compute_area_list')

    @api.model
    def _default_lines(self):
        area_obj_ids = self.env['recursos_materiales.area'].search([])
        lines = []
        numero_areas = len(area_obj_ids)
        if numero_areas:
            for i in range(0, 5):
                lines.append((0, 0, {
                    'area_id': area_obj_ids[random.randint(0, numero_areas - 1)].id
                }))
        return lines

    @api.model
    def _default_area(self):

        """"
        Arch             DB
        id1             id1 -> idDB 100
        id2             id2 -> idB 506

            ir_model
            ir_params
            ir_module
            ir_
            base_
        """
        areas = {
            1: self.env.ref('recursos_materiales.qps_rma_area_tesoreria').id,
            2: self.env.ref('recursos_materiales.qps_rma_area_presupuestos').id,
            3: self.env.ref('recursos_materiales.qps_rma_area_cajas').id,
            4: self.env.ref('recursos_materiales.qps_rma_adquisiciones').id,
            5: self.env.ref('recursos_materiales.qps_rma_area_atencion').id,
        }
        return areas.get(random.randint(1, 50), False)

    @api.model
    def _compute_area_list(self):
        content_html = ""
        for area_id in self.env['recursos_materiales.area'].search([]):
            content_html += """
                  <li>{}</li>
            """.format(area_id.nombre)
        content_html = "<ul>{}</ul>".format(content_html)
        for record in self:
            record.area_list = content_html

    @api.onchange('oficio_linea_ids')
    def _compute_total(self):
        for linea in self:
            linea.total = 0
            for registro in linea.oficio_linea_ids:
                linea.total += registro.subtotal
                print(registro.area_id)
                # self.env.context = self.with_context(registro_area_id=registro.area_id).env.context

    @api.onchange('oficio_linea_ids.nombre')
    def _onchange_oficio_linea_ids_nombre(self):
        for linea in self.oficio_linea_ids:
            print(linea.nombre, linea.area_id)

    @api.depends()
    def _depends_empleado(self):
        return {
            'domain': {
                'empleado': [('login', '!=', self.env.user.login)]
            }
        }

    @api.onchange('nombre_solicitante')
    def _onchange_nombre_solicitante(self):
        # search([('','','')])
        self.nombre_solicitante = str(self.nombre_solicitante).upper()
        if len(self.nombre_solicitante) <= 3:
            raise exceptions.ValidationError('Capture el nombre del solicitante')

    def _compute_nombre_mostrar(self):
        for registro in self:
            registro.nombre_mostrar = str(registro.folio) + ' ' + str(registro.name)

    @api.constrains('correo_solicitante')
    def _constrains_validacion_correo(self):
        self.env['lib.validaciones'].sudo().correo(self.correo_solicitante)

    # Actions
    @api.multi
    def action_pendiente(self):
        self.ensure_one()
        self.state = 'pendiente'  # Orm -> SQL
        if self.area_id:
            if self.area_id.folio:
                self.folio = self.area_id.folio.next_by_id() or "/"
        elif self.id > 5:
            self.folio = self.env['ir.sequence'].next_by_code('recursos_materiales.oficio') or '/'
        else:
            self.folio = self.env['ir.sequence'].next_by_code('recursos_materiales.oficio_generico') or '/'

    @api.multi
    def action_pendiente_contestar(self):
        self.ensure_one()
        self.write({'state': 'pendiente_contestar'})  # Sql -> Orm
        # self.env.cr.execute("select, delete, update  * from recursos_materiales_oficio")

    @api.multi
    def action_finalizar(self):
        self.ensure_one()
        self.update({'state': 'finalizado'})  # Orm -> SQL

    @api.multi
    def action_cancelar(self):
        self.ensure_one()
        self.state = 'cancelado'

    @api.multi
    def action_rechazar(self):
        self.ensure_one()
        self.state = 'rechazado'

    def f1(self, p1, p2):
        # p1=hola p2=hola2
        pass

    def f2(self, *p1):
        # ['hola', 'hola2']
        pass

    def f3(self, **p1):
        # {'p1':hola, 'p3':'hola2'}
        pass

    def f4(self, p1, **kwargs):
        pass

    def f5(self, p1, *args):
        pass

    @api.multi
    def action_abrir_form_area(self):
        return {
            "name": "Recursos Materiales Area",
            "type": "ir.actions.act_window",
            "view_mode": "tree,form",
            "res_model": "recursos_materiales.area"
        }

    """ C#
    class RMO {
        RMO(){
            this.suma(1,2)
            // this  -> self en python 
        }
        suma([no existe this], p1, p2){
                return p1 +p2
        }
    }
    """

    """
         @api.multi 
         write 
         unlink
     """

    @api.model
    def create(self, vals):
        """ ondiciones """
        # nombre_solicitante = vals.get('nombre_solicitante', "")
        # self.validar_nombre_solicitante(nombre_solicitante)
        data = {
            'categoria_id': [[6, False, []]],
            'fecha': False, 'write_uid': False,
            'asunto': 'prueba 4', 'empleado': False, 'correo_solicitante': False,
            'oficio_linea_ids': [
                [0, 'virtual_61', {'nombre': 'prueba', 'categoria_id': 1, 'cantidad': 1, 'precio': 120}],
                [0, 'virtual_64', {'nombre': False, 'categoria_id': False, 'cantidad': False, 'precio': False}],
                [0, 'virtual_67', {'nombre': False, 'categoria_id': False, 'cantidad': False, 'precio': False}],
                [0, 'virtual_70', {'nombre': False, 'categoria_id': False, 'cantidad': False, 'precio': False}],
                [0, 'virtual_73', {'nombre': False, 'categoria_id': False, 'cantidad': False, 'precio': False}]],
            'nombre_solicitante': 'FALSE', 'moneda_id': 34, 'create_uid': False,
            'area_id': False, 'state': 'borrador', 'evidencia': [[6, False, []]], 'create_date': False,
            'write_date': False, 'queja': '<p><br></p>'}

        # import pdb; pdb.set_trace()
        print(vals, self.env.context)
        id = super(RecursosMaterialesOficio, self).create(vals)
        if vals.get('oficio_linea_ids', False) and len(vals.get('oficio_linea_ids')):
            id._compute_total()
            id.write({'total': id.total})
        return id

    @api.multi
    def write(self, vals):
        print(vals)
        id = super(RecursosMaterialesOficio, self).write(vals)
        # [ (1,0, {'codigo_cat': 'nuevo valor'}), ]
        if vals.get('oficio_linea_ids', False) and len(vals.get('oficio_linea_ids')):
            self._compute_total()
            self.write({'total': self.total})
        return id

    """
        public function recursos_materiates_area get_area(self)
            
    """

    @api.returns('recursos_materiales.area')
    def get_area(self):
        return self.area_id

    @api.model
    def get_imagenes(self):
        imagenes = []
        if self.evidencia:
            for imagen in self.evidencia:
                imagenes.append({'datas': imagen.datas.decode('utf-8')})
        return imagenes


"""
class RecursosMaterialesOficio(models.Model):
    _name = 'recursos_materiales.oficio'
    #permite indicar nuevo campo que sera el valor a mostrar
    _rec_name = 'nombre_mostrar'

    # datos generales
    folio = fields.Char(string=_("Folio"))
    #*****Como se puede usar una serie folio distinta como para claseificar por area o categoria

    #El valor de name sera el valor default a mostrar al menos que se declare un _rec_name
    #name = fields.Char(string=_("Nombre"), size=20)
    #state = 
    #active = 
    name = fields.Char(string=_("Nombre"), required=True, default="Oficio: ....")

    #Generales
    fecha = fields.Date(string=_("Fecha"))
    empleado = fields.Many2one(comodel_name='res.users', string=_("Empleado"),)
    #*****Se puede poner en un valor default el resultado de una consulta
    #*****llenar en automatico con los oficios registrados con el mismo correo
    oficios_empleado = fields.Integer(string=_("Oficios del empleado"))

    nombre_mostrar = fields.Char(compute="_compute_nombre_mostrar")

    def _compute_nombre_mostrar(self):
        for registro in self:
            registro.nombre_mostrar = "Folio:" + str(registro.folio) + "-" + str(registro.name) + "-" +\
                                      str(registro.empleado.name)
            # *****En vez de poner el nombre del empleado pone el id
            # registro.nombre_mostrar = registro.name +"-"+registro.empleado

    # datos solicitante
    nombre_solicitante = fields.Char(string=_("Nombre del solicitante"))

    #*****Validar mascara o por expresion regular sintaxis de correo
    correo_solicitante = fields.Char(_("Correo electronico del solicitante"))
    @api.onchange('correo_solicitante')
    @api.constrains('correo')
    def _correo(self):
        if False:
            raise  exceptions.ValidationError("")
        return 
    
    #*****llenar en automatico con los oficios registrados con el mismo correo
    oficios_solicitante = fields.Integer(string=_("Oficios del solitante"))

    #Queja
    asunto = fields.Text(string=_("Asunto"), default="Describe brevemente el  ....")

    # *****Catalogos de categoria y area
    # area = fields.Many2one(comodel_name = '', string=_("Area"))
    # categoria = fields.Many2one(comodel_name = '', string=_("Categoria"))
    # ***** agregar un listado simple para poner nivel de queja o ranquear por leve, moderada, media, fuerte, inmediata
    #****** esta se deberia llenar por quien la recibe
    #queja = fields.Char(_("Queja"))
    queja = fields.Html(_("Queja"))
    """

"""
imagen = fields.Binary(_("Foto"))

# Auditoria
#*****validar si tiene datos no reemplazar
dbcapcur = fields.Char(string=_("Cursor de base de datos"), default=lambda self: self.env.cr, readonly="1")
usercap = fields.Char(string=_("Usuario captura"), default=lambda self: self.env.uid, readonly="1")
datecap = fields.Char(string=_("Fecha captura"), default=fields.Date.today, readonly="1")
instancecap = fields.Char(string=_("Instancia"), default=lambda self: self.env['recursos_materiales.oficio'], readonly="1")
contextcap = fields.Char(string=_("Contexto"), default=lambda self: self.env.context, readonly="1")
modelcap = fields.Many2one('res.user', string=_("Modelo"), default=lambda self: self.env['recursos_materiales.oficio'],readonly="1")
user_id = fields.Many2one('res.users', string="Usuario id", default=lambda self: self.env.user, readonly="1")
#*****ip :??
#*****mac: ??

#*****siempre que modifica actualiza
date_mod = fields.Datetime(string=_('Fecha modificación'), readonly=True, default=fields.Datetime.now())
usermod = fields.Char(string=_("Usuario captura"), default=lambda self: self.env.uid, readonly="1")

# *****agregar una encuesta simple de atención al contribuyente y calcular el % de satisfacción

"""
