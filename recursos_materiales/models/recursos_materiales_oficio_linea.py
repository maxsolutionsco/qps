# -*- coding: utf-8 -*-

from odoo import models, fields, api, _, exceptions
from odoo.addons import decimal_precision as dp


class RecursosMaterialesOficioLinea(models.Model):
    _name = 'recursos_materiales.oficio_linea'

    oficio_id = fields.Many2one('recursos_materiales.oficio',
                                string=_("Referencia oficio"))

    nombre = fields.Char(string=_("Nombre"))

    area_id = fields.Many2one(comodel_name="recursos_materiales.area",
                              string=_("Área"))

    categoria_id = fields.Many2one(comodel_name="recursos_materiales.categoria",
                                   string=_("Categoría"))

    cantidad = fields.Integer(string=_("Cantidad"))
    precio = fields.Float(string=_("Precio"), digits=dp.get_precision('Recursos Materiales'))

    # 10.00005
    subtotal = fields.Float(string=_("Subtotal"), compute="_compute_subtotal_one",
                            digits=dp.get_precision('Recursos Materiales'))

    @api.depends('cantidad', 'precio')
    @api.one  # r1 6s
    def _compute_subtotal_one(self):
        # precios = self.precios_existentes()
        self.subtotal = self.cantidad * self.precio

    @api.multi  # [r1, r2,r3 ... ] 2s
    def _compute_subtotal_multi(self):
        # precios = self.precios_existentes()
        for registro in self:
            registro.subtotal = registro.cantidad * registro.precio

    @api.model
    def precios_existentes(self):
        self.env.cr.execute("select * from product_product")  # 2s
        return []

    @api.onchange('categoria_id')
    def _onchage_categoria_id(self):
        print(self.area_id, self.nombre)
    """
        public, protected, private 
        var = 1         
        _var = 1
        __var = 1 
    """
