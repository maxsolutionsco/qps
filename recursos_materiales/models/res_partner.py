from odoo import models, fields, api, _, exceptions


class ResPartner(models.Model):
    _inherit = 'res.partner'
    # OP2 _rec_name = 'name2'

    codigo = fields.Char(string=_("Codigo"))
    alias = fields.Char(string=_("Alias"))
    company_type = fields.Selection(selection=[('person', 'Fisica'), ('company', 'Moral')])

    primer_nombre = fields.Char(string='Primer nombre')
    segundo_nombre = fields.Char(string='Segundo nombre')
    apellido_paterno = fields.Char(string='Apellido paterno')
    apellido_materno = fields.Char(string='Apellido materno')
    nombre_razon_social = fields.Char(string='Nombre o razón social')
    # name2 = fields.Char(string="Nombre", compute="_concatena_nombre")
    """
    @api.depends('primer_nombre', 'segundo_nombre', 'apellido_paterno', 'apellido_materno', 'nombre_razon_social')
    def _concatena_nombre(self):
        for registro in self:
            if registro.nombre_razon_social:
                xnombre = str(registro.nombre_razon_social) if registro.nombre_razon_social else ' Sin Nombre'
                registro.name = xnombre
            else:
                xnombre = str(registro.apellido_paterno) + ' ' + str(registro.apellido_materno) + ' ' + str(
                    registro.primer_nombre) + ' ' + str(registro.segundo_nombre)
            # xnombre = registro.cat_apellido_paterno + ' ' + registro.cat_apellido_materno + ' ' + registro.cat_primer_nombre + ' ' + registro.cat_segundo_nombre
                registro.name = xnombre
    """
    # OP1
    # @api.model
    # def get_name(self):
    # if self.customer ==true or self.supplier =True

    # @api.model
    # def create(self, vals):
    #    vals.update({'name': self.nombre_razon_social})
    #    return super(ResPartner, self).create(vals)
