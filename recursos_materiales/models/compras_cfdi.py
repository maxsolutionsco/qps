# -*- coding: utf-8 -*-
from odoo import models, fields


class ComprasCfdi(models.Model):
    _inherit = 'purchase.order'
    archivo_cfdi_xml = fields.Binary(string="Archivo CFDI XML")
