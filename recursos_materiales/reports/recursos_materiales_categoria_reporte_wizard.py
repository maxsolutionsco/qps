# -*- coding: utf-8 -*-
from odoo import models, api, fields, exceptions, _


class RecursosMaterialesCategoriaReporteWizard(models.TransientModel):
    #_name = 'lib.wizard.categoria'
    _inherit = 'lib.wizard'

    codigo = fields.Char(string=_("Código"))

    @api.multi
    def exportar_csv(self,registros=False, campos=[], encabezados=[], nombre_archivo=False,modelo=False):
        if self.tipo_reporte == 'recursos_materiales.categoria.reporte':
            condicion = []
            condicion = condicion + self.obtener_rango_fechas()
            if self.codigo:
                condicion.append(('codigo', 'ilike', str(self.codigo)))
            registros = self.env['recursos_materiales.categoria'].search(condicion)
            campos = ['nombre', 'codigo']
            encabezados = ["Nombre", "Código"]
            nombre_archivo = "Reporte categorias"
            modelo = 'recursos_materiales.categorias.reporte'
            print("Categorias")
        return super(RecursosMaterialesCategoriaReporteWizard, self).exportar_csv(
            registros=registros,
            campos=campos,
            encabezados=encabezados,
            nombre_archivo=nombre_archivo,
            modelo=modelo
        )
