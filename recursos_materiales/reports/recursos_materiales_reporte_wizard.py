# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions, _
import base64


class RecursosMaterialesReporteWizard(models.TransientModel):
    _name = 'lib.wizard.area'
    _inherit = 'lib.wizard'

    @api.multi
    def exportar_csv(self, registros=False, campos=[], encabezados=[], nombre_archivo=False, modelo=False):
        if self.tipo_reporte == 'recursos_materiales.area.reporte':
            condicion = []
            # condicion += self.obtener_rango_fechas()
            # condicion = condicion + self.obtener_rango_fechas()
            condicion = list(set(condicion + self.obtener_rango_fechas()))
            areas = self.env['recursos_materiales.area'].search(condicion)
            """ if len(areas):
     
            #datas = areas.export_data(['nombre', 'folio', 'active'])
            # {'datas':[[..],[...],[...]]}
            #filas = datas.get('datas')  # [[...],[...]]
            #csv = "Nombre, Folio, Estatus\n"
            #for fila in filas:
            #    for valor in fila:
            #        csv += str(valor) + ","
            #    csv += "\n"
            #archivo_base64 = base64.b64encode(bytes(csv.encode('utf-8')))
          
            nombre_archivo = 'Reporte areas.csv'
            archivo_area_ids = self.env['ir.attachment'].sudo().search([
                ('res_model', '=', modelo)
            ])
            if len(archivo_area_ids) > 0:
                archivo_area_ids.unlink()
            archivo_area_id = self.env['ir.attachment'].sudo().create({
                'name': nombre_archivo,
                'datas_fname': nombre_archivo,
                'type': 'binary',  # url, store
                'datas': archivo_base64,
                'res_model': modelo,  # recursos_materiales.oficio
                # 'res_id': ''  #  10
                'mimetype': 'text/csv'  # application/csv text/html ....
            })
           
            Nombre	 Folio	 Estatus
            demos	Recursos Materiales Area Generico	True
            Area 2	False	True
            Tesorería	False	True
            Presupuestos	False	True
            Cajas	False	True
            Adquisiciones	False	True
            Atencion	Recursos Materiales Area Generico	True

           
            return {
                'type': 'ir.actions.act_url',
                'url': '/web/content?id={}&download=True'.format(archivo_area_id.id),
                'target': 'self'  # new https://qps.com
            } """
            registros = areas
            campos = ['nombre', 'folio', 'active']

            # "%s" % (p1 )
            # '%s' % (p1 )
            encabezados = ["Nombre", "Folio", "Estatus"]
            nombre_archivo = "Reporte areas"
            modelo = 'recursos_materiales.area.reporte'
            # import pdb; pdb.set_trace()
            print("Area")
        return super(RecursosMaterialesReporteWizard, self).exportar_csv(
            registros=registros,
            campos=campos,
            encabezados=encabezados,
            nombre_archivo=nombre_archivo,
            modelo=modelo
        )
