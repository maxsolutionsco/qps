{
    'name': "Recursos Materiales",

    'summary': """
        """,

    'author': "QPS",
    'website': "http://qps.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'crm',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','mail', 'decimal_precision', 'contacts','lib','purchase'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'data/folio.xml',
        'data/precision_decimal.xml',
        'data/area.xml',
        'data/formato_papel_data.xml',
        'views/recursos_materiales_oficio_views.xml',
        'views/recursos_materiales_area_views.xml',
        'views/recursos_materiales_categoria_views.xml',
        'views/res_partner_views.xml',
        'reports/recursos_materiales_reporte_wizard.xml',
        'reports/recursos_materiales_categoria_reporte_wizard.xml',
        'views/compras_cfdi_views.xml',
        'views/archivos_estaticos.xml',
        'reports/recursos_materiales_oficio_reporte_pdf.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo/demo.xml',
    ],
}
# -*- coding: utf-8 -*-
