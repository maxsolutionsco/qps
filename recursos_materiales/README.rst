Recursos Materiales
===================


Notebook tabs - scroll

.. image:: static/img/tabs-scroll.png
   :width: 600

Guia de desarrollo
https://www.python.org/dev/peps/pep-0008/

Editor archivos RST
http://rst.ninjs.org/

Editor archivo PO
https://localise.biz/free/poeditor

2018-11-27
----------


* css addons/web/static/src


2018-11-26
----------
* q-web


2018-11-{21,22,23}
------------------
* Wizard
* Inherit

2018-11-20
----------
* Search
* Security


2018-11-16
----------
* Model ORM
    * Browse
* Graph view
* Pivot view

2018-11-15
----------

Editores online
https://aws.amazon.com/es/cloud9/
https://codeshare.io
https://bitbucket.org/snippets
https://gist.github.com

* Model ORM
    * ref
    * env
    * with_context

2018-11-14
----------

* Model ORM
    * unlink
    * write
    * copy
* Kanban view


2018-11-13
-----------
Icons https://es.slideshare.net/TaiebKristou/odoo-icon-smart-buttons
Icons https://fontawesome.com/?from=io

* Fields
    * Integer copy
    * Float get_precision
    * Monetary currency_field
* Model
    * APi
        * Multi
        * one
        * model
        * depends
        * return

2018-11-12
-----------------------
Widgets https://es.slideshare.net/CelineGeorge1/widgets-in-odoo

* Form view
    * Many2many
        * widget
            * checkbox
            * tags
            * radio
        * editable
            * bottom
* Field
    * One2many

2018-11-09
-----------------------
* Field attribute
    * related
* Field
    * Many2one
    * Many2many
* Model
    * _table
    * _description
    * __sql_constraints
    * Methods
        * search
        * search_count
    * fields
        editable [bottom, top]

    * API
        * onchange
        * constrains

2018-11-08
-----------------------
* Field attribute
    * invisible
    * domain
    * readonly
    * required
* Tree view
    * default_order
    * create
    * edit
    * colors


07 Noviembre 2018
-----------------------
* Form view
    * header
    * button
    * widget statusbar
    *
* Tree view
* Model
    * _inherit
    * Methods
        * write
        * Update
* Field attribute
    * tracking_visibility [always, onchange]

06 Noviembre 2018
-----------------------
* Menus
* act_window
* field
    * Char
    * Date
    * Datetime
    * Many2one
    * Text
    * compute
* Model
    * _name
    * _rec_name



05 Noviembre 2018
-----------------------
* odoo-bin
    * scaffold crear modulos
    * -u actualizar un modulo
    * -d seleccionar una base de datos
    * --xmlrpc-port definir puerto de conexion
    * --addons-path agregar nueva carpeta de addons
